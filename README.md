# Makex #

Makefile Extraction (Makex) extracts the conditions under which each C files get compiled in linux. The constraints are presented as propositional formula.

Instructions on how to use it are available at http://www.sarahnadi.org/research/kbuildvariability